FROM alpine:3.8 as sources
RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh

RUN git clone https://github.com/alextselegidis/easyappointments.git /easyappointments


FROM node:18-alpine3.14 as installer
COPY --from=sources /easyappointments /easyappointments 
WORKDIR /easyappointments
RUN npm install

FROM composer:latest as installer2
COPY --from=installer /easyappointments /app/easyappointments
WORKDIR /app/easyappointments
RUN composer install --ignore-platform-reqs

FROM php:7.4-apache
COPY --from=installer2 /app/easyappointments /var/www/html
COPY ./config.php /var/www/html/config.php
WORKDIR /var/www/html
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd gettext mysqli pdo_mysql
RUN a2enmod rewrite
EXPOSE 80
RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"
RUN chmod a+w /var/www/html/storage