<?php

class Config {
    const BASE_URL      = 'http://localhost:8080';
    const LANGUAGE      = 'english';
    const DEBUG_MODE    = TRUE;

    const DB_HOST       = 'db';
    const DB_NAME       = 'easyappointments';
    const DB_USERNAME   = 'root';
    const DB_PASSWORD   = 'root';

    const GOOGLE_SYNC_FEATURE   = FALSE;
    const GOOGLE_PRODUCT_NAME   = '';
    const GOOGLE_CLIENT_ID      = '';
    const GOOGLE_CLIENT_SECRET  = '';
    const GOOGLE_API_KEY        = '';
}